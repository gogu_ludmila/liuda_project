<?php
namespace tests;

use ForTests\SimpleCalculator;
use PHPUnit\Framework\TestCase;

class SimpleCalculatorTest extends TestCase
{
    /**
     * @return array
     */
    public function dataForAddition(): array
    {
        return [
            'TwoPositiveNumbersTwoAndTwo' => [
                'a' => 2,
                'b' => 2,
                'expectedResult' => 4,
            ],
            'TwoPositiveNumbersTwoAndThree' => [
                'a' => 2,
                'b' => 3,
                'expectedResult' => 5,
            ],
            'TwoPositiveNumbersThreeAndTwo' => [
                'a' => 3,
                'b' => 2,
                'expectedResult' => 5,
            ],
            'TwoPositiveNumbersFiveAndNineteen' => [
                'a' => 5,
                'b' => 19,
                'expectedResult' => 24,
            ],
            'NegativeNumbersFiveAndNineteen' => [
                'a' => -5,
                'b' => -19,
                'expectedResult' => -24,
            ],
            'VeryLargeNumbers' => [
                'a' => PHP_INT_MAX,
                'b' => PHP_INT_MAX,
                'expectedResult' =>  1.8446744073709552E+19,
            ],
        ];
    }

    /**
     * @dataProvider dataForAddition
     */
    public function testAddTwoNumbers($a, $b, $expectedResult): void
    {
        $calculator = new SimpleCalculator();
        $this->assertEquals($expectedResult, $calculator->add($a, $b));
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function dataForSubtraction(): array
    {
        return [
            'TwoPositiveNumbersTwoAndTwo' => [
                'a' => 2,
                'b' => 2,
                'expectedResult' => 0,
            ],
            'two negative numbers two and two' => [
                'a' => -2,
                'b' => -2,
                'expectedResult' => 0,
            ],
            'five and nineteen' => [
                'a' => 5,
                'b' => 19,
                'expectedResult' => -14,
            ],
        ];
    }

    /**
     * @dataProvider dataForSubtraction
     *
     * @param int $a - the number that will be subtracted from
     * @param int $b - amount that will be subtracted
     * @param $expectedResult
     */
    public function testSubtractTwoNumbers(int $a, int $b, $expectedResult): void
    {
        $calculator = new SimpleCalculator();
        $this->assertEquals($expectedResult, $calculator->subtract($a, $b));
    }


    public function dataDivideTwoNumbers(): array
    {
        return [
            'one and one' => [
                'a' => 1,
                'b' => 1,
                'expectedResult' => 1,
            ],
            'two and one' => [
                'a' => 2,
                'b' => 1,
                'expectedResult' => 2,
            ],
            'one and zero' => [
                'a' => 1,
                'b' => 0,
                'expectedResult' => 1.0E+33,
                'precisionWasLost' => true
            ],
            '99999 by 33333' => [
                'a' => 99999,
                'b' => 33333,
                'expectedResult' => 3,
                'precisionWasLost' => false
            ],
            '100000 by 3' => [
                'a' => 100,
                'b' => 3,
                'expectedResult' => 33.333333333333334,
                'precisionWasLost' => false
            ],
        ];
    }
    /**
     * @dataProvider dataDivideTwoNumbers
     */
    public function testDivideTwoNumbers(int $a, int $b, $expectedResult, bool $precisionWasLost = false): void
    {
        $calculator = new SimpleCalculator();
        $this->assertEquals($expectedResult, $calculator->divide($a, $b));
        $this->assertEquals(!$precisionWasLost, $calculator->isPrecizeResult());
    }






    public function dataMultiplyTwoNumbers(): array
    {
        return [
            'one and one' => [
                'a' => 1,
                'b' => 1,
                'expectedResult' => 1,
            ],
            'one hundred and 111111111' => [
                'a' => 100,
                'b' => 111111111,
                'expectedResult' => 11111111100,
            ],

        ];
    }
    /**
     * @dataProvider dataMultiplyTwoNumbers
     */
    public function testMultiplyTwoNumbers(int $a, int $b, $expectedResult, bool $precisionWasLost = false): void
    {
        $calculator = new SimpleCalculator();
        $this->assertEquals($expectedResult, $calculator->multiply($a, $b));
    }

}
