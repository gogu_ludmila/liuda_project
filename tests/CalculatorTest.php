<?php
namespace Tests;

use ForTests\Calculator;
use PHPUnit\Framework\TestCase;


class CalculatorTest extends TestCase
{

    public function dataForAddition(): array
    {
        return [
            'testAddTwoPositiveNumbersTwoAndTwo' => [
                'a' => 2,
                'b' => 2,
                'expectedResult' => 4,
            ],
            'testAddTwoPositiveNumbersTwoAndThree' => [
                'a' => 2,
                'b' => 3,
                'expectedResult' => 5,
            ],
            'testAddTwoPositiveNumbersThreeAndTwo' => [
                'a' => 3,
                'b' => 2,
                'expectedResult' => 5,
            ],
            'testAddTwoPositiveNumbersFiveAndNineteen' => [
                'a' => 5,
                'b' => 19,
                'expectedResult' => 24,
            ],
            'testAddNegativeNumbersFiveAndNineteen' => [
                'a' => -5,
                'b' => -19,
                'expectedResult' => -24,
            ],
            'testAddTwoNegativeNumberFiveAndFive'=>[
                'a'=>-5,
                'b'=>-5,
                'expectedResult'=>-10,
            ]

        ];
    }

    /**
     * @dataProvider dataForAddition
     */
    public function testAddTwoNumbers($a, $b, $expectedResult): void
    {
        $calculator = new Calculator();
        $this->assertEquals($expectedResult, $calculator->add($a, $b));
    }



    public function dataForSubtraction(): array
    {
        return [
            'testSubtractTwoPositiveNumbersTwoAndTwo' => [
                'a' => 2,
                'b' => 2,
                'expectedResult' => 0,
            ],
            'testSubtractTwoNegativeNumbersTwoAndTwo' => [
                'a' => -2,
                'b' => -2,
                'expectedResult' => 0,
            ],
            'testSubtractFiveAndNineteen' => [
                'a' => 5,
                'b' => 19,
                'expectedResult' => -14,
            ],
        ];
    }

    /**
     * @dataProvider dataForSubtraction
     */
    public function testSubtractTwoNumbers($a, $b, $expectedResult): void
    {
        $calculator = new Calculator();
        $this->assertEquals($expectedResult, $calculator->subtract($a, $b));
    }
}






//class Calculator
//{
//    public function add($a, $b)
//    {
//        return $a + $b;
//    }
//
//
//
//    public function subtract($a, $b)
//    {
//        return $a - $b;
//    }
//
//    public function multiplication($a, $b){
//
//        return $a * $b;
//
//    }
//}
