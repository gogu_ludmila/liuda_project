<?php

namespace ForTests;

class RestrictedCalculator implements Icalculator
{
    /**
     * @var SimpleCalculator $simpleCalculator
     */
    private $simpleCalculator;

    /**
     * RestrictedCalculator constructor.
     *
     * @param SimpleCalculator $simple
     */
    public function __construct(SimpleCalculator $simple)
    {
        $this->simpleCalculator = $simple;
    }

    public function add($a, $b)
    {
        return $this->simpleCalculator->add($a, $b);
    }

    public function subtract($a, $b)
    {
        return $this->simpleCalculator->subtract($a, $b);
    }

    public function divide($a, $b)
    {
        return $this->simpleCalculator->divide($a, $b);
    }

    /**
     * @param $a
     * @param $b
     * @return int
     * @todo use other implementation
     *
     */
    public function multiply($a, $b)
    {
        $result = 0;
        for ($i = 0; $i < $b; $i++) {
            $result += $a;
        }

        return $result;
    }
}