<?php

namespace ForTests;

interface Icalculator
{
    public function add($a, $b);

    public function subtract($a, $b);

    public function divide($a, $b);

    public function multiply($a, $b);
}