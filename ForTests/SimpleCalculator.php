<?php

namespace ForTests;

class SimpleCalculator implements Icalculator
{
    private $precisionWasLost;

    public function __construct()
    {
        $this->precisionWasLost = false;
    }

    public function add($a, $b)
    {
        return $a + $b;
    }

    /**
     * @param $a
     * @param $b
     * @return mixed
     */
    public function subtract($a, $b)
    {
        return $a - $b;
    }

    public function divide($a, $b)
    {
        if ($b === 0) {
            $this->precisionWasLost = true;
            $this->precisionLostReason = 'division by zero';

            $b = 0.000000000000000000000000000000001;
        }

        return $a / $b;
    }

    public function multiply($a, $b)
    {
        return $a * $b;
    }


    public function isPrecizeResult()
    {
        return !$this->precisionWasLost;
    }
}
